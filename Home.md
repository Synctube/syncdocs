Installation (v3.0 - CURRENT): https://github.com/calzoneman/sync/wiki/CyTube-3.0-Installation-Guide

User Guide (v3.0 - CURRENT): https://github.com/calzoneman/sync/wiki/CyTube-3.0-User-Guide

FAQ: https://github.com/calzoneman/sync/wiki/Frequently-Asked-Questions

Guidelines for issue reporting: https://github.com/calzoneman/sync/wiki/Reporting-an-Issue

Installation (v2.4.6 - OUTDATED): https://github.com/calzoneman/sync/wiki/Installing

Beginner's Guide (v2.x - OUTDATED): https://github.com/calzoneman/sync/wiki/Beginner%27s-Guide-and-FAQ

CyTube Bot: - https://github.com/nuclearace/CytubeBot