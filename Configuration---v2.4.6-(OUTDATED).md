The following page aims to assist you in configuring your CyTube server.  Prior to following these instructions, you should have installed the software using the guidelines in the [Installing](https://github.com/calzoneman/sync/wiki/Installing-2.4) wiki page.

## MySQL Config

CyTube requires a MySQL database.  We will create a new user for CyTube to authenticate as (I **strongly** advise against using the root account for your CyTube database user)

```plain
mysql -u root -p
(Enter password)
```

```MySQL
> GRANT USAGE ON *.* TO some_user@localhost IDENTIFIED BY 'some_password';
> CREATE DATABASE sync_db;
> GRANT ALL PRIVILEGES ON sync_db.* TO some_user@localhost;
```

Make sure you remember the username (`some_user`), the password (`some_password`), and the database name (`sync_db`), you'll need it later for the CyTube configuration file.

## CyTube Server Configuration

Run the server once to generate a template `cfg.json` file (it will warn you that it's generating a new configuration file): `node index.js`

Below are explained the configuration keys:
- `mysql-server` - The host address of the MySQL database.  If you installed MySQL to the same machine on which you're installing CyTube, use `localhost` or `127.0.0.1`
- `mysql-db` - The MySQL database that CyTube has permission to use
- `mysql-user` - The MySQL user that CyTube should authenticate as
- `mysql-pw` - Password for `mysql-user`
- `express-host` - If your server has multiple IPv4 addresses, you can specify which address CyTube should bind to.  The default (`0.0.0.0`) binds to all available IPs.
- `io-host` - Equivalent to `express-host`, except for socket.io connections
- `enable-ssl` - Whether or not to listen for secure HTTPS and WSS connections
- `ssl-keyfile` - Path to `.key` file for your SSL certificate
- `ssl-passphrase` - Passphrase to unlock your SSL key
- `ssl-certfile` - Path to `.crt` file for your SSL certificate
- `ssl-port` - Port to listen on for SSL.  Binding port 443 requires the application to be run as root.  I use port 8443, and set up an iptables rule to forward traffic from port 443 to 8443.
- `asset-cache-ttl` - Allows you to set the duration that clients may cache web assets (in milliseconds)
- `web-port` - The port to use for HTTP traffic **NOTE:** binding to port 80 requires root permissions
- `io-port` - The port to use for Socket.IO traffic **NOTE:** some firewalls block websockets on port 8080
- `ip-connection-limit` - The maximum number of Socket.IO connections that may be open at a time from the same IP address
- `guest-login-delay` - Number of seconds required to wait between guest logins
- `channel-save-interval` - Interval at which to save all channels (in minutes).  Default every 5 minutes.  Note that channels should be saved automatically on graceful shutdown; this feature is in case of unexpected shutdowns.
- `trust-x-forward` - Read clients' IP addresses from the `X-Forwarded-For` header.  Only use this if you know what you are doing, because if you're not actually behind a proxy this can be used to fake IP addresses
- `enable-mail` - Allows you to disable/enable using `nodemailer` to send password reset links
- `mail-transport`, `mail-config` - See documentation for [nodemailer](https://github.com/andris9/Nodemailer)
- `mail-from` - When sending password reset emails, the address to send in the `From:` field
- `domain` - When sending password reset links, the domain to use as the base name.  Reset links are of the form `domain/reset.html?reset_hash`
- `ytv3apikey` - API key for using the YouTube v3 API
- `enable-ytv3` - Uses YouTube v3 API for single video queries.  v2 is still used for playlists and searches.
- `ytv2devkey` - Developer key for YouTube v2 API (not required - uses anonymous API by default)
- `stat-interval` - Interval (in milliseconds) at which to record statistics datapoints
- `stat-max-age` - Maximum age (in milliseconds) of a stat point before it is deleted
- `alias-purge-interval` - Interval (in milliseconds) at which aliases older than `alias-max-age` will be cleared from the database
- `alias-max-age` - Maximum age (in milliseconds) of an alias (ip, name, time triplet) record before it is deleted from the database

## CyTube client configuration

Now that your server's configured, we just need to change a few values so the client knows where to connect.  Open up `www/assets/js/iourl.js` in your favourite text editor.

- Update `IO_URL` to use your server's address and the port number you chose as `io-port`
- Update `WEB_URL` to use your server's address and the port number you chose as `web-port`
- If you are allowing SSL, set `ALLOW_SSL` to `true` and update `SSL_URL` with the appropriate details

Example:
```javascript
var IO_URL = "http://synch.somedomain.com:8080";
var WEB_URL = "http://synch.somedomain.com:8089";
var SSL_URL = "http://synch.somedomain.com:8443";
var ALLOW_SSL = true;
```

## Post-configuration

You should now have a working CyTube server!  Start it up by executing `node index.js`.  You can also use the provided `run.sh` file to auto-restart the server if it crashes.

To launch it without needing to keep your terminal open, you can use screen (`screen ./run.sh`), tmux, nohup, or similar tools.

Navigate to your server address (the value of `WEB_URL`) and click Account.  Click Register and create an account for yourself.  To make this account an administrator (say the name is "admin"), do the following:

```plain
mysql -u some_user -p sync_db
(enter password)
```
```MySQL
> UPDATE registrations SET global_rank=1000 WHERE uname='admin';
```

Any rank >= 255 is a site administrator.

## Maintenance

CyTube is under development and frequently updates.  To apply these updates, you can use git to fetch the latest changes from the repository.  Execute `git pull` from the sync directory, and restart the server.  If you get an error message, you may need to run an update script (`node update.js`) or update the node dependencies (`npm update`).  Be sure to read the commit messages on GitHub to see what's changing, and whether any administrator intervention is required.

The administration control panel (located at `acp.html`) provides a convenient overlook of the server to administrators.  You can view log files, statistics, users, channels, and more.


## Help

If you need help, come to IRC: http://webchat.6irc.net/?channels=cytube