# CyTube 3.0 User Guide

## ! ##

This user guide is currently being rewritten in order to improve its accuracy and usefulness.  Before reading below, check whether the information you need is covered in the [new user guide](https://github.com/calzoneman/sync/blob/3.0/docs/index.md).

## Table of Contents
  1. [What's New](#wiki-whats-new)
  2. [Channel UI](#wiki-channel-ui)
  3. [Channel Management](#wiki-channel-management)
  4. [User settings](#wiki-user-settings)
  5. [Chat Commands](#wiki-chat-commands)
  6. [Supported Media Providers](#wiki-supported-media-providers)
  7. [Help/Support](#wiki-help--support)

## What's new
  * UI Changes
    * Updated bootstrap to v3.0
    * Redesigned playlist controls
    * Redesigned channel moderation settings
    * Added HD layout
    * Added Slate and Cyborg themes
    * Moved guest login form to the chat box
    * Added alias list to user profile box (for moderators)
  * Webserver changes
    * Changed login system
      * Allows you to be logged in from more than one device
      * Permits more secure storage of authentication data
    * Changed account management system
  * New channel features
    * Builtin emote support
    * Private messaging

## Channel UI

Each channel page consists of several basic sections: the chat box, the video, the playlist controls, the playlist, and the poll area.

#### Chat box

The chat box allows you to communicate with the other users in the channel.  At the top, there is a bar with the number of connected users.  Clicking this will hide the userlist to expand the message area.

The userlist consists of all the names of users in the channel who are logged in.  Right click a name to pop up available actions for that user.  If you are not a moderator, you can ignore the user or send them a private message.  Moderators can use this menu to kick, ban, and perform other moderating tasks.

At the bottom, there is a chat input bar.  If you are not logged in, it will prompt you to choose a guest name.  After logging in, simply type your message in the input box and press enter to send it.  Certain types of messages will be interpreted as commands, which will be covered below.

#### Playlist controls

Below the video, there are 2 strips of buttons for playlist and video control.  Depending on your permission level, certain buttons may be disabled.  Hover over each button to see what it does.

On the left are playlist controls:
  * Search for a video: Search for a video title in the channel library or on YouTube
  * Add from URL: Paste a supported media link and add it to the playlist
  * Embed a custom frame: Embed an `<iframe>` or `<object>` tag for media that is not officially supported
  * Manage playlists: Save and load playlists from your user account
  * Clear playlist: Remove all videos from the playlist
  * Shuffle playlist: Randomize the order of the playlist and start playing from the top
  * Lock/Unlock playlist: Toggle the locked status of the playlist.  Unlocked state is useful for allowing non-moderators to add videos to the playlist.  Exact open/locked permissions can be configured under the channel settings permissions editor

On the right are video controls:
  * Reload the video player: Remove the current video and load it again (useful for dealing with playback issues)
  * Retrieve playlist links: Get a comma-separated list of all the videos in the playlist
  * Voteskip: Vote to skip the currently playing video.  If enough votes are counted, the next item in the playlist will immediately begin playing

#### Playlist

The playlist is a list of videos that will be played in order from top to bottom.  Each entry will display the title and duration of the video.  Each item has a few buttons to perform certain actions, the location of which depends on your user preferences.  Click and drag playlist items to rearrange the order.

Item actions:
  * Play: Immediately jump to this playlist item and begin playing
  * Queue Next: Move this video to the next slot after the currently playing video
  * Make Temporary/Permanent: Toggle whether this video will be automatically removed from the playlist after playing once
  * Delete: Remove this item from the playlist

#### Poll area

The poll area allows moderators to conduct polls.  When a moderator opens a poll, a poll box will be added to the poll area.  Each option in the poll has a button next to it for you to select that option.  Each IP address can only vote once, and your vote is cleared when you leave the page.

The buttons next to each option will display the number of votes for that option.  If it displays "?", then the poll opener has elected to hide the poll results until the poll is over.

## Channel Management

You must be logged in to your account to manage your channels.

### Registering

Go to Account -> Channels.  On the right column, enter the desired channel name and click "Register".  If that channel is not already registered to another account, it will be created and registered to your name.  The website administrator may limit the number of channels that can be registered to one account.

### Deleting

Go to Account -> Channels.  The left column lists all channels registered to your name.  Click the "Delete" button to unregister a channel.  This will permanently clear all database information (ranks, library, bans), and state information (playlist, configuration, MOTD, filters, etc) and is not reversible.  You will be asked to confirm your deletion.

### Channel Settings

From your channel page (/r/channelname), users with moderator or higher rank will see a "Channel Settings" button on the top navigation bar.  This opens a dialog which allows you to manage various aspects of your channel, explained below.

#### General Settings

  * Convert URLs in chat to links: If enabled, URLs entered into chat (e.g. http://google.com) will be converted to clickable hyperlinks
  * Allow voteskip: If enabled, users may click the voteskip button to indicate a preference to skip the currently playing video
  * Voteskip ratio: The proportion of votes from non-AFK users required to skip the current video.  The number of votes required is calculated as `ceil(voteskip ratio * number of non-AFK users)`
  * Max video length: Specify the maximum duration of a playlist item.  Set this to `00:00:00` to allow unlimited length
  * Auto-AFK Delay: After this many seconds of a user sending no chat messages and not voteskipping, the user will be automatically marked as "AFK"
  * Throttle chat: Enables a flood filter that will limit how quickly a user can send chat messages
  * # of messages allowed before throttling: When Throttle chat is active, the number of messages a user can send with no time delay before the flood filter engages
  * # of messages allowed per second: After the above limit is exceeded, only this many messages are allowed per second.  Additional messages are ignored.

#### Admin Settings

  * Page title: The title displayed on the browser window / tab
  * Password: An optional password required for non-moderators to enter the room.  Leave blank to disable
  * External CSS: URL to an external stylesheet to apply to the page
  * External Javascript: URL to an external script to run on the page
  * List channel publicly: When enabled, display the channel name on the index page.  Only active channels (channels with at least 1 user) will be displayed

#### Edit / Chat Filters

Chat filters provide a way for certain text to be recognized and changed in chat messages.  For example, it could be used to transform curse words to strings of asterisks.  DO NOT USE CHAT FILTERS FOR IMAGE EMOTES.  Use the Emotes feature instead.

**Adding a new filter**

  * Filter name: A name to identify the filter.  For informational purposes only, but must be unique
  * Filter regex: A regular expression describing the text to match
  * Flags: A set of regular expression flags to apply.  `g` means to match all instances in the message (instead of just the first), `i` means to match without case sensitivity.
  * Replacement: The HTML that will replace the matched text.

See the MDN page for [RegExp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp) for reference on Regular Expressions and flags.

**Managing existing filters**

Filters will be applied in the order they are listed.  You can click and drag individual filters to rearrange them.  The "Active" checkbox allows you to easily enable/disable individual filters.  Under the "Control" column, the left button with a list icon opens an editor to change the regular expression, flags, and replacement for a filter.  The red button with a trash icon will delete the filter.

The "Filter Links" checkbox controls whether this filter will be applied to links in chat.  By default, this is off, to prevent filters from breaking links pasted in chat by changing them.

**Importing/Exporting filters** (New in CyTube 3.0)

You can export and import your channel's filter list in order to back it up or transfer it to another room.  Clicking Export filter list will populate the textarea with a JSON-encoded list of filter data.  To import a filter list, paste the JSON data in the textbox and click Import filter list.  The import tool is very strict on format and will reject invalid filters, so please be careful to store the exported data verbatim.

#### Edit / Emotes (New in CyTube 3.0)

Channel emotes provides an easy way to replace certain words in chat with an image.  Each image will be rendered at a maximum of 200px by 200px.  If this is too large, you can use channel CSS to set the `max-width` and `max-height` properties of the `.channel-emote` CSS class, or you can scale your images before uploading them.  For example, if you want all emotes to have a maximum width and height of 70px:

```css
.channel-emote
{
  max-width: 70px;
  max-height: 70px;
}
```

The emote editing interface is similar to chat filters, except the emote names are automatically converted from plain text to regular expressions.  Click on the text containing the image URL to change the image for an emote.

#### Edit / MOTD

The MOTD is a block at the top of the page where you can display a Message of the Day to your channel, for example a header image, rules, or important announcements.  The MOTD editor accepts HTML formatting, but will filter out any attempts to execute Javascript.

#### Edit / CSS

The CSS editor allows you to add up to 20,000 characters of inline CSS (as opposed to External CSS, which requires you to host the file elsewhere).  Use this to apply custom styles or colors to your channel.

#### Edit / Javascript

The Javascript editor allows you to add up to 20,000 characters of inline Javascript (as opposed to External Javascript, which requires you to host the file elsewhere).  Use this to run Javascript on your page, for example to make layout changes to the page or add your own timer.

#### Edit / Permissions

The permissions editor provides a way for you to configure what rank is required to carry out certain actions.  Each permission option has a dropdown for selecting the minimum rank required to perform that action.

Since there is sometimes some confusion over "Open Playlist" vs. "General Playlist", I'll note the difference here:  When the playlist is unlocked (the button below the video is green with a checkmark), the Open Playlist Permissions will take precedence over the same permissions under General Playlist Permissions.

#### Edit / Moderators

This menu allows you to manage the moderators of the channel.  To add a new moderator, enter their username in the text field and click "+Mod" for moderator rank, "+Admin" for channel admin rank, or "+Owner" for channel owner rank.  Moderators have most channel moderation permissions (by default), channel admins have additional permissions such as promoting users to moderators and editing certain channel settings, and channel owners are equivalent to channel admins except they can also promote and demote administrators and owners.

In the moderator list, you can use the Edit dropdown to change a user's rank, or remove them from the moderator list.  This dropdown will be disabled for users who have higher rank than you.

#### Ban list

The ban list shows a table of ban entries for the channel.  Each entry consists of a name, partially obscured IP address (or `*` if only the username was banned), the name of the moderator who added the ban, and optionally a note on why the ban was added.  Hover over a row to reveal the ban reason.

#### Log

The log allows you to see chat history as well as moderation actions, playlist changes, and other data about your channel.  Above the log text, there is a multiple select box that you can use to display only certain kinds of log data (e.g. chat, joins/quits, playlist actions, moderation actions, etc.).

## User settings

At the top of the page, you should see a button labeled "Options".  This will open a dialog that allows you to configure various personal settings.  These are divided into 4 categories.

### General

#### Theme

The theme selector allows you to choose a different colorscheme for the site.  By default there are 4 themes.  Slate and Cyborg are both dark themes, Light is a flat, light colored scheme, and Bootstrap is the same colors as Light but with added gradients.

#### Layout

The layout selector changes how the elements on the page are positioned and sized.  Compact and Fluid have chat on the left, video on the right, and polls and the playlist below.  Synchtube and Synchtube + Fluid are the same as Compact and Fluid except reversed- chat is on the right with video on the left.  Fluid and Synchtube + Fluid will expand the chat and video to fill the width of your screen, whereas Compact has a static size.  HD places video on top, then the playlist and chat side by side, then polls below chat.

#### Ignore Channel CSS

CyTube includes the ability to style channels with CSS.  However, you can use this option to override these changes and use the default CSS.

#### Ignore Channel Javascript

This is analagous to Ignore Channel CSS.  If enabled, it will not run any custom javascript from the channel.  This is useful for recovering your channel if you make a mistake in your Javascript that makes it impossible to revert.

#### Encrypt connection with SSL

This option is deprecated.  Instead, the client will automatically connect using SSL if the server supports it.

### Playback

#### Synchronize video playback

This checkbox controls whether or not your video will be synchronized with the server.

#### Synch threshold

This is the number of seconds representing how close your video will be synchronized with the server.  For example, with the default value (2), your video will only be forced to re-sync if it is at least 2 seconds ahead or behind the server.  Smaller values indicate more accurate synchronization, but may cause a lot of unnecessary seeking interrupting the video.  If you're having trouble with a slow connection, you may want to increase this setting to 5 or 10 seconds.

#### Set wmode=transparent

The `wmode=transparent` parameter on the video player allows elements to be rendered over the video like normal.  However, on some implementations of Flash (e.g. Linux), this is extremely laggy, so I added this option to force an opaque `wmode`.

#### Use Flash for h.264 playback

Since Vimeo banned cytu.be from embeds, Vimeo playback is currently supported via direct links to h.264 encoded video files.  Some browsers, including Chrome and as of version 29, Firefox, support native h.264 playback via the `<video>` tag.  However, others must rely on a third-party Flash plugin to decode the video.  CyTube attempts to autodetect whether your browser supports native playback, but if Vimeo videos aren't loading, you can try manually enabling this setting.

#### Remove the video player

This option will automatically remove the video player every time you load a channel.  Useful if you're using CyTube on a computer or in a location where you don't want the video present.  You can do a 1 time removal of the video by clicking Layout -> Remove Video from the top of the page.

#### Hide playlist buttons by default

Instead of "Play / Queue Next / Make Temporary / Delete" buttons being visible on every playlist item, they will start out hidden.  You can then right click a playlist item to expand the buttons, and right click it again to hide them.

#### Old style playlist buttons

This is a legacy feature for people who prefer the icon-style playlist buttons from CyTube 1.0.

#### Default YouTube quality

This dropdown allows you to select the quality parameter passed to the player when it loads a video.

### Chat

#### Show timestamps in chat

If enabled, prepends a timestamp of the form [HH:MM:SS] to the beginning of each chat message with the time it was sent.

#### Sort userlist by rank

If enabled, all admins will be sorted to the top of the userlist, followed by moderators, followed by regular users, followed by guests.  Within each rank group, names are sorted in alphabetical order.

#### Sort AFKers to bottom

If enabled, users marked as AFK will appear at the bottom of the userlist.

#### Blink page title on new messages

This option controls whether the page title will flash (alternates between chatroom title and `*Chat*`) when certain conditions occur.  If set to 'always', it will flash on every new message.  If set to 'only when I am mentioned or PMed', it will only flash if your name is present in the message, or the message is a PM.  If set to 'never', the page title will not be flashed for any message.  Please note this applies only when CyTube is not the active window.

#### Blink page title on new messages

Same as above, but for playing a notification sound.

#### Add a send button to chat

This adds an extra button below the chat box you can click to send a message.  This is for devices with soft keyboards that don't have an enter key (because apparently those exist?)

### Moderator

#### Show name color

Color your username when you send a chat message, based on your rank.

#### Show join messages

Show a small message whenever a user joins the channel.

#### Show shadowmuted messages

When a user is shadowmuted, their chat is only visible to other shadowmuted users by default.  If you check this option, you will see messages from shadowmuted users, but dimmed and with a strikethrough.

## Chat Commands

### Standard commands

  * `/me <message>` - Sends a message as an action, for example *calzoneman gets a snack*.
  * `/sp <message>` - Hides a message in a hover-to-expose spoiler box.
  * `/afk` - Toggles your AFK status.  You will automatically be marked not AFK after sending a chat message or voteskipping the video.

### Moderator commands
  * `/say <message>` - Sends a message in big red text.
  * `/poll <title>,<option1>,<option2>,...` - Opens a new poll (this is from before the New Poll button was added).
  * `/hpoll <title>,<option1>,<option2>,...` - Same as above, but hides poll results.
  * `/mute <user>` - Mutes a user from sending chat messages.
  * `/smute <user>` - Shadow mutes a user.  They will still receive their own chat messages, but nobody else will and they will not appear muted except to moderators.
  * `/unmute <user>` - Unmutes a user.  This will unmute both muted and smuted users.
  * `/kick <user> [<reason>]` - Kicks a user, optionally providing a kick reason
  * `/kickanons` - Kicks all anonymous users from the channel
  * `/ban <user> [<reason>]` - Bans a user by name, optionall providing a ban reason
  * `/ipban <user> [<reason>]` - Bans a user and all IP addresses they've used in the past month, optionally providing a ban reason.
  * `/ipban <user> range [<reason>]` - Bans an IP range (/24 block for IPv4 or /64 block for IPv6)
  * `/ipban <user> wrange [<reason>]` - Bans a wider IP range (/16 block for IPv4, /48 block for IPv6)
  * `/clear` - Clears the chat buffer.
  * `/clean <user>` - Removes all playlist entries from the given user.
  * `/cleantitle <title>` - Removes all playlist entries that match the given title.
  * `/d <message>` - Calls a drink with the given message
  * `/d# [<message>]` - Calls # drinks, optionally with a message.  # can be positive or negative.

### Default formatting

By default, channels come with a few default chat filters that can be used for formatting:

  * `_message_` - *message*
  * `*message*` - **message**
  * `` `message` `` - `message` (monospace, code text)
  * `~~message~~` - ~~message~~

##  Supported Media Providers
See the [FAQ](https://github.com/calzoneman/sync/wiki/Frequently-Asked-Questions#which-media-providers-are-supported).

## Help / Support

If you need help, feel free to join [irc.6irc.net#cytube](http://webchat.6irc.net/?channels=cytube).  calzoneman is the developer, nuclearace is a CyTube admin, and many other people in the channel can help with common questions.