In order for me to best handle an issue, please try to provide as much of the following information as possible:

- The channel and host on which you're experiencing the issue (e.g. http://cytu.be/r/blah)
- The browser and version you're using (e.g. Firefox 21)
- What caused the problem to happen (e.g. I changed a certain setting and then my channel exploded)

Before opening an issue, please try the following steps which solve common problems:

- Refresh
- Clear cache / cookies
- Disable extensions
- Try another browser

If your issue is solved by disabling extensions or using a different browser, please include the browsers/extensions which do not work in your issue report.

If your question includes the words "doesn't work", I can guarantee that I will respond with "what do you mean 'doesn't work'?"