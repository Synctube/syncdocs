## Step 1: Install dependencies

### Debian 7
```plain
# apt-get update && apt-get upgrade
# apt-get install build-essential git python
# apt-get install mysql-server
```

### CentOS
Contributed by https://github.com/coconono

```sh
$ yum install git openssl-devel gcc-c++
$ yum install mysql-server mysql mysql-devel mysql-libs
```

Editor's Node: I'm not sure all of those mysql packages are necessary, I'll update the Wiki if I get a chance to test a fresh CentOS machine

### Ubuntu

```sh
$ sudo apt-get install build-essential git mysql-server
```

## Step 2: Install node
- Retrieve the latest sources from http://nodejs.org/
```plain
$ tar xf node-v0.10.(version #).tar.gz
$ cd node-v0.10.(version #)
$ ./configure
$ make
$ sudo make install
$ cd ..
```

## Step 3: Install CyTube
```plain
$ git clone https://github.com/calzoneman/sync
$ cd sync
$ npm install
```

## Step 4: Configuration
- See the wiki page for [Configuration](https://github.com/calzoneman/sync/wiki/Configuration)